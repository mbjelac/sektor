import {Game} from "./engine/start-new-game";

export interface GameState {
  player: {displayName: string};
  game?: Game;
}

export const gameState: GameState = {

  player: {
    displayName: 'Esmeralda'
  },

  game: undefined,
};
