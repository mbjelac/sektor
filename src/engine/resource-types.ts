export const R_CONSTRUCTION_BLOCK = "resource_constructionBlock";
export const R_SOLAR_PANEL = "resource_solarPanel";
export const R_PIPES = "resource_pipes";
export const R_ELECTRONICS = "resource_electronics";

export const R_ENERGY_ELECTRICAL = "electricity";
export const R_WATER = "water";
