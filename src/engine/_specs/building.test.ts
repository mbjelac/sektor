import {startNewGame} from "../start-new-game";
import {getConstructableBuildingTypes} from "../tool-strip";
import {getCellLocation} from "../cells";
import {selectCell} from "../select-cell";
import {BuildingType} from "../building-types";
import {setConstructablesOnCellType} from "../config";
import * as fixture from "./_fixtures";



describe("types available for construction are", () => {

  beforeAll(() => {
    setConstructablesOnCellType(BuildingType.B_TERRAIN, BuildingType.B_HIVE, BuildingType.B_SPARK_CATCHER);
  })

  afterAll(() => {
    setConstructablesOnCellType(BuildingType.B_TERRAIN);
  })

  test('empty when non-terrain cell selected', () => {

    const game = fixture._start_game_().game;

    const nonEmptyCellLocation = getCellLocation(game, cell => cell.buildingType !== BuildingType.B_TERRAIN);

    selectCell(game, nonEmptyCellLocation);

    expect(getConstructableBuildingTypes(game)).toEqual([]);
  });

  test('set as configured when terrain cell selected', () => {

    const game = fixture._start_game_().game;

    const emptyCellLocation = getCellLocation(game, cell => cell.buildingType === BuildingType.B_TERRAIN);

    selectCell(game, emptyCellLocation);

    expect(getConstructableBuildingTypes(game)).toEqual([BuildingType.B_HIVE, BuildingType.B_SPARK_CATCHER]);
  });
});
