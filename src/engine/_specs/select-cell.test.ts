import {Cell, Game, getRandomCoord} from "../start-new-game";
import {CellLocation, getCellAt} from "../cells";
import {getSelectedCell} from "../tool-strip";
import {isLocationSelected, selectCell} from "../select-cell";
import * as fixture from "./_fixtures";


describe('selecting cell', () => {

  let game: Game;
  let cell1: Cell;
  let cell2: Cell;
  let location1: CellLocation;
  let location2: CellLocation;

  beforeEach(() => {
    game = fixture._start_game_().game;
    [cell1, location1] = aRandomCell(game);
    [cell2, location2] = aRandomCell(game);
  });

  test('updates selected cell reference', () => {

    selectCell(game, location2);

    expect([
      isLocationSelected(game, location1),
      isLocationSelected(game, location2)
    ]).toEqual([
      false,
      true
    ]);
  });

  test('display info strip when cell is selected', () => {

    selectCell(game, location2);

    expect(getSelectedCell(game)).toEqual(cell2);
  });

  describe("which does not exist", () => {

    const nonExistentLocation: CellLocation = new CellLocation(-1, -1);

    it("does not change selected cell reference", () => {

      selectCell(game, location1);
      selectCell(game, location2);
      selectCell(game, nonExistentLocation);

      expect([isLocationSelected(game, location1), isLocationSelected(game, location2)]).toEqual([false, true]);
    });
  });
});

function aRandomCell(game: Game): [Cell, CellLocation] {
  const location: CellLocation = new CellLocation(getRandomCoord(), getRandomCoord());
  return [getCellAt(game, location)!, location];
}

