import * as fixture from "./_fixtures";
import {StartedGame} from "./_fixtures";
import {BuildingType} from "../building-types";
import {Cell, ConstructionSpecificationsMap, Game, GameConfiguration} from "../start-new-game";
import {CellLocation, getCellAt} from "../cells";

let startedGame: StartedGame;

const someBuildingType = BuildingType.B_POND;

describe("starting construction of building", () => {

  it("creates construction cell", () => {
    const given = givenBuildStarted();
    expect(given.cell.buildingType).toEqual(BuildingType.B_CONSTRUCTION);
  });

  it('selects the new cell', () => {

    const given = givenBuildStarted();

    expect(given.location.isEqual(given.game.selectedLocation)).toBeTruthy();
  });

  test('building a construction site is not allowed', () => {

    expect(() => givenBuildStarted({
      typeToConstruct: BuildingType.B_CONSTRUCTION
    })).toThrow("not allowed: construction");
  });
});

describe("completing construction of building", () => {

  it("not finished if not enough time passed", () => {
    const given = givenBuildStarted({ duration: 6 })

    fixture._time_passes_(startedGame, 5);

    const cell = getCellAt(given.game, given.location)!;
    expect(cell.buildingType).toEqual(BuildingType.B_CONSTRUCTION);
  });

  it("replaces construction with constructed building", () => {
    const given = givenBuildStarted({
      duration: 6,
      typeToConstruct: BuildingType.B_GARDEN
    });

    fixture._time_passes_(startedGame, 6);

    const cell = getCellAt(given.game, given.location)!;
    expect(cell.buildingType).toEqual(BuildingType.B_GARDEN);
  });
});

it("constructing fails if no construction kits", () => {
  fail("TODO");
});

it("building not allowed on occupied cell", () => {

  const given = givenBuiltCell();

  expect(() => fixture._build_a_cell_(startedGame, someBuildingType, given.location))
    .toThrow('Cell is occupied!');
});

interface BuildContext {
  duration?: number,
  typeToConstruct?: BuildingType
}

interface GivenContext {
  game: Game;
  cell: Cell;
  location: CellLocation;
}

function givenBuildStarted(context?: BuildContext): GivenContext {

  const duration: number = context?.duration || 1;
  const typeToConstruct: BuildingType = context?.typeToConstruct || someBuildingType;

  const configuration: GameConfiguration = {
    constructionSpecifications: new ConstructionSpecificationsMap([
      typeToConstruct, { duration }
    ])
  };

  startedGame = fixture._start_game_(configuration);

  const location = fixture._getEmptyCellCoords_(startedGame.game);

  return {
    game: startedGame.game,
    cell: fixture._build_a_cell_(startedGame, typeToConstruct, location),
    location
  };
}

function givenBuiltCell(): GivenContext {

  const context = givenBuildStarted();

  fixture._time_passes_(startedGame, 1);

  return {
    ...context,
    cell: getCellAt(context.game, context.location)!
  };
}