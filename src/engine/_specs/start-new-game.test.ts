import {startNewGame} from "../start-new-game";
import {CellLocation, getAllCells, getCell, getCellLocation} from "../cells";
import {R_CONSTRUCTION_BLOCK} from "../resource-types";
import {INITIAL_CAPSULE_STORAGE, MAP_SIZE} from "../config";
import {isLocationSelected} from "../select-cell";
import {BuildingType} from "../building-types";
import * as fixture from "./_fixtures";

test('initial map is correct size', () => {

  const game = fixture._start_game_().game;

  expect(game.map.cells.length).toEqual(MAP_SIZE);
});

test('initial map is square', () => {

  const game = fixture._start_game_().game;

  const height = game.map.cells.length;

  game.map.cells
    .map(row => row.length)
    .forEach(rowWidth => expect(rowWidth).toEqual(height));
});

test('cell 0,0 is selected', () => {

  const game = fixture._start_game_().game;

  expect(new CellLocation(0,0).isEqual(game.selectedLocation)).toBeTruthy();
});

test('initial map has only a single terminal', () => {

  const game = fixture._start_game_().game;

  const cells = getAllCells(game);

  expect(cells.filter(cell => cell.buildingType === BuildingType.B_TERRAIN).length).toEqual(cells.length - 1);
  expect(cells.filter(cell => cell.buildingType === BuildingType.B_TERMINAL).length).toEqual(1);
});

test('terminal is placed in a random location', () => {

  const distinctCoords = new Set();

  for (let i = 0; i < 10; i++) {

    const { x, y } = getCellLocation(fixture._start_game_().game, cell => cell.buildingType === BuildingType.B_TERMINAL);

    distinctCoords.add(x + "," + y);
  }

  expect(distinctCoords.size).toBeGreaterThan(1);
});
