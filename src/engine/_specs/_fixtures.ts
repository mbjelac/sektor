import {Cell, ConstructionSpecificationsMap, Game, GameConfiguration, startNewGame} from "../start-new-game";
import {CellLocation, getCell, getCellAt, getCellLocation} from "../cells";
import {BuildingType} from "../building-types";
import {build} from "../build";
import {selectCell} from "../select-cell";
import {tick} from "../tick";

const DEFAULT_BUILDING_TYPE = BuildingType.B_GARDEN;

export interface StartedGame {

  game: Game;
  capsule: Cell;
}

const defaultGameConfiguration: GameConfiguration = {
  constructionSpecifications: new ConstructionSpecificationsMap()
};

export function _start_game_(configuration: GameConfiguration = defaultGameConfiguration): StartedGame {

  const game = startNewGame(configuration);

  const capsule = getCell(game, cell => cell.buildingType === BuildingType.B_TERMINAL);
  return { game, capsule };

}

export function _build_a_cell_(startedGame: StartedGame, buildingType: BuildingType = DEFAULT_BUILDING_TYPE, coords?: any): Cell {

  const game = startedGame.game;

  coords = specifiedOrEmptyCell(game, coords);

  selectCell(game, coords);

  build(game, buildingType);

  return getCellAt(game, coords)!;
}

function specifiedOrEmptyCell(game, coords?: CellLocation): CellLocation {
  return coords || _getEmptyCellCoords_(game);

}

export function _time_passes_(startedGame: StartedGame, numberOfTicks: number = 1): void {
  for (let i = 0; i < numberOfTicks; i++) {
    tick(startedGame.game);
  }
}

export function _getEmptyCellCoords_(game: Game): CellLocation {
  return getCellLocation(game, cell => cell.buildingType === BuildingType.B_TERRAIN);

}
