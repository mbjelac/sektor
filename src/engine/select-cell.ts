import {CellLocation, getCellAt} from "./cells";
import {Cell, Game} from "./start-new-game";

export function isLocationSelected(game: Game, cellLocation: CellLocation) {
  return cellLocation.isEqual(game.selectedLocation);
}

export function selectCell(game: Game, location: CellLocation) {

  if (!getCellAt(game, location)) {
    return;
  }

  game.selectedLocation = location;
}
