import {ConstructionSite} from "./construction-site";
import {BuildingType} from "../building-types";

const anyType: BuildingType = BuildingType.B_GARDEN;

it("indicates when duration reached", () => {

  const construction = new ConstructionSite(anyType, 3);

  expect([
    construction.construct(),
    construction.construct(),
    construction.construct()
  ]).toEqual([
    false,
    false,
    true
  ]);
});

it("throws after duration reached", () => {

  const construction = new ConstructionSite(anyType, 3);

  construction.construct();
  construction.construct();
  construction.construct();

  expect(() => construction.construct()).toThrow();
});