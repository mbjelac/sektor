import {BuildingType} from "../building-types";
import {Cell} from "../start-new-game";
import {CellLocation} from "../cells";

export class ConstructionSite {
  constructor(
    readonly buildingType: BuildingType,
    private duration: number
  ) {
  }

  construct(): boolean {

    if (this.duration === 0) {
      throw new Error("Construction site already finished work!");
    }

    this.duration--;

    return this.duration <= 0;
  }

}