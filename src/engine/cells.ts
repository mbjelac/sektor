import {Cell, Game} from "./start-new-game";

export function getAllCells(game: Game): Cell[] {

  const array: Cell[] = [];

  game.map.cells.forEach(row => array.push(...row));

  return array;
}

export class CellLocation {

  constructor(
    readonly x: number,
    readonly y: number) {

  }

  isEqual(other?: CellLocation): boolean {

    if (!other) {
      return false;
    }

    return other.x === this.x && other.y === this.y;
  }
}

export type CellPredicate = (cell: Cell) => boolean;

export function getCellLocation(game: Game, cellTest: CellPredicate): CellLocation {

  const cellMatrix = game.map.cells;

  for (let y = 0; y < cellMatrix.length; y++) {
    for (let x = 0; x < cellMatrix[y].length; x++) {
      if (cellTest(cellMatrix[y][x])) {
        return new CellLocation(x, y);
      }
    }
  }

  throw new Error("Cell not found!");
}

export function getCell(game: Game, cellTest: CellPredicate): Cell {

  const { x, y } = getCellLocation(game, cellTest);

  return game.map.cells[y][x];
}

export function getCellAt(game: Game, location: CellLocation): Cell | undefined {

  const row = game.map.cells[location.y];

  return !!row ? row[location.x] : undefined;
}

export function setCellAt(game: Game, location: CellLocation, cell: Cell) {

  game.map.cells[location.y][location.x] = cell;
}
