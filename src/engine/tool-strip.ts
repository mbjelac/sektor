import {BuildingType} from "./building-types";
import {Cell, Game} from "./start-new-game";
import {getConstructablesOnCellType} from "./config";

export function getSelectedCell(game: Game): Cell | undefined {

  if (!game.selectedLocation) {
    return undefined;
  }

  return game.map.getCellAt(game.selectedLocation);
}

export function getConstructableBuildingTypes(game: Game): BuildingType[] {

  const selectedCell = getSelectedCell(game);

  if (!selectedCell) {
    return [];
  }

  return getConstructablesOnCellType(selectedCell.buildingType);
}

