export type ResourceType = string;

export interface ResourceState {
  readonly total: number;
  amount: number;
}

export interface TypedResourceState {
  type: ResourceType;
  state: ResourceState;
}

export class Resources {

  static from(...resourceTypesAndTotals: [ResourceType, number][]): Resources {
    return new Resources(
      new Map(
        resourceTypesAndTotals
          .filter(([, total]) => total > 0)
          .map(
            ([type, total]) =>
              [
                type,
                {
                  total,
                  amount: 0
                }
              ]
          )
      )
    );
  }

  private constructor(private readonly storage = new Map<ResourceType, ResourceState>()) {
  }

  change(resourceType: ResourceType, amount: number): void {

    const existingState = this.storage.get(resourceType);

    if (!existingState) {
      return;
    }

    existingState.amount = within(
      0,
      existingState.amount + amount,
      existingState.total
    );
  }

  getAll(): TypedResourceState[] {
    return Array
      .from(this.storage)
      .map(([type, state]) =>
        ({ type, state }));
  }
}

function within(min: number, value: number, max: number): number {
  return Math.max(min, Math.min(max, value));
}

export interface ResourcesSpecification {
  getAll(): ([ResourceType, number])[];
}

export class ResourcesSpecificationMap implements ResourcesSpecification {

  private readonly map = new Map<ResourceType, number>();

  constructor(...items: ([ResourceType, number])[]) {
    items.forEach(([type, amount]) => this.map.set(type, amount));
  }

  getAll(): [ResourceType, number][] {
    return Array.from(this.map);
  }
}