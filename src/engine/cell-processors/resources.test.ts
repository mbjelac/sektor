import {Resources, ResourceState, ResourceType} from "./resources";

type ResourceListing = [ResourceType, ResourceState][];

let resources: Resources;

describe('creating resources', () => {

  it('with nothing in them', () => {

    resources = Resources.from();

    thenResourcesContain();
  });

  it('initialises current amounts to 0', () => {

    resources = Resources.from(
      ["foo", 42],
      ["bar", 12],
      ["pop", 23],
    )

    thenResourcesContain(
      ["foo", { total: 42, amount: 0 }],
      ["bar", { total: 12, amount: 0 }],
      ["pop", { total: 23, amount: 0 }]
    );
  });

  it("ignores non-positive totals", () => {

    resources = Resources.from(
      ["foo", 0],
      ["bar", -12],
      ["pop", -0.00001],
      ["zap", 0.00001],
    );

    thenResourcesContain(["zap", { total: 0.00001, amount: 0 }]);
  });
});

describe('adding to resources', () => {

  it('changing amount of type', () => {

    resources = Resources.from(["foo", 10]);

    resources.change("foo", 2);
    resources.change("foo", 3);

    thenResourcesContain(["foo", { total: 10, amount: 5 }]);
  });

  it('does not bother other types', () => {

    resources = Resources.from(
      ["foo", 10],
      ["bar", 10],
    );

    resources.change("foo", 2);

    thenResourcesContain(
      ["foo", { total: 10, amount: 2 }],
      ["bar", { total: 10, amount: 0 }],
    );
  });

  it("adds only up to total", () => {

    resources = Resources.from(
      ["foo", 10],
      ["bar", 20],
    );

    resources.change("foo", 2);
    resources.change("foo", 6);
    resources.change("foo", 7);
    resources.change("bar", 100);

    thenResourcesContain(
      ["foo", { total: 10, amount: 10 }],
      ["bar", { total: 20, amount: 20 }],
    );
  });

  it("subtracts only down to 0", () => {
    resources = Resources.from(
      ["foo", 10],
      ["bar", 20],
    );

    resources.change("foo", 2);
    resources.change("foo", -6);
    resources.change("foo", -7);
    resources.change("bar", -100);

    thenResourcesContain(
      ["foo", { total: 10, amount: 0 }],
      ["bar", { total: 20, amount: 0 }],
    );
  });
});

function thenResourcesContain(...listing: ResourceListing): void {
  expect(resources.getAll().map(typedState => [typedState.type, typedState.state])).toEqual(listing);
}
