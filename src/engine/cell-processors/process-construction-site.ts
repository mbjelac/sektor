import {CellLocation} from "../cells";
import {CellMap} from "../start-new-game";

export function processConstructionSite(cells: CellMap, location: CellLocation) {

  const constructionCell = cells.getCellAt(location);

  const shouldConstruct = constructionCell!.constructionSite!.construct();

  if (!shouldConstruct) {
    return;
  }

  cells.setCellAt(location, { buildingType: constructionCell!.constructionSite!.buildingType });
}
