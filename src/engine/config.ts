import {ResourceType} from "./cell-processors/resources";
import {BuildingType} from "./building-types";

export const MAP_SIZE = 10;

export const INITIAL_CAPSULE_STORAGE =
  new Map();

const INITIAL_CONSTRUCTION_SITE_MISSING_RESOURCES = new Map();

export function setInitialMissingResource(buildingType, resourceType, resourceAmount) {

  if (!INITIAL_CONSTRUCTION_SITE_MISSING_RESOURCES.has(buildingType)) {
    INITIAL_CONSTRUCTION_SITE_MISSING_RESOURCES.set(buildingType, new Map());
  }

  INITIAL_CONSTRUCTION_SITE_MISSING_RESOURCES.get(buildingType).set(resourceType, resourceAmount);
}

export function getInitialMissingResources(buildingType) {

  return INITIAL_CONSTRUCTION_SITE_MISSING_RESOURCES.get(buildingType);
}

const BUILDING_CONFIGURATIONS = new Map<BuildingType, BuildingConfiguration>();

export function setBuildingConfiguration(type: BuildingType, config: BuildingConfiguration) {

  BUILDING_CONFIGURATIONS.set(type, config);
}

export interface ResourceThroughput {
  input: Map<ResourceType, number>,
  output: Map<ResourceType, number>
}

export interface BuildingConfiguration {
  throughputCoefficients: ResourceThroughput,
  throughputTotals: ResourceThroughput
}

export class BuildingConfigurationBuilder {

  private readonly coefficients: ResourceThroughput = {
    input: new Map<ResourceType, number>(),
    output: new Map<ResourceType, number>()
  };

  private readonly totals: ResourceThroughput = {
    input: new Map<ResourceType, number>(),
    output: new Map<ResourceType, number>()
  };

  private addToCoefficients = true;
  private addToInputs = true;

  throughputCoefficients(): BuildingConfigurationBuilder {
    this.addToCoefficients = true;
    return this;
  }

  throughputTotals(): BuildingConfigurationBuilder {
    this.addToCoefficients = false;
    return this;
  }

  inputs(): BuildingConfigurationBuilder {
    this.addToInputs = true;
    return this;
  }

  outputs(): BuildingConfigurationBuilder {
    this.addToInputs = false;
    return this;
  }

  add(type: ResourceType, value: number): BuildingConfigurationBuilder {

    const throughput = this.addToCoefficients
      ? this.coefficients
      : this.totals;

    const map =
      this.addToInputs
        ? throughput.input
        : throughput.output;

    map.set(type, value);

    return this;
  }

  build(): BuildingConfiguration {
    return {
      throughputCoefficients: this.coefficients,
      throughputTotals: this.totals
    };
  }
}


const emptyConfig: BuildingConfiguration = {
  throughputTotals: {
    input: new Map(),
    output: new Map()
  },
  throughputCoefficients: {
    input: new Map(),
    output: new Map()
  }
};

export function getBuildingConfiguration(type: BuildingType): BuildingConfiguration {

  return BUILDING_CONFIGURATIONS.get(type) || emptyConfig;
}

const constructableOnCellType = new Map<BuildingType, BuildingType[]>();

export function getConstructablesOnCellType(type: BuildingType): BuildingType[] {
  return constructableOnCellType.get(type) || [];
}

export function setConstructablesOnCellType(type: BuildingType, ...constructables: BuildingType[]): void {
  constructableOnCellType.set(type, constructables);
}
