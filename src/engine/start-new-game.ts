import {BuildingType} from "./building-types";
import {BuildingConfigurationBuilder, MAP_SIZE, setBuildingConfiguration} from "./config";
import {selectCell} from "./select-cell";
import {createCell} from "./create-cell";
import {CellLocation, setCellAt} from "./cells";
import {ResourcesSpecification} from "./cell-processors/resources";
import {R_ENERGY_ELECTRICAL, R_WATER} from "./resource-types";
import {ConstructionSite} from "./construction/construction-site";

export interface Game {
  readonly map: CellMap;
  readonly configuration: GameConfiguration;
  selectedLocation?: CellLocation;
}

export class CellMap {

  // FIXME: cells should not be public
  constructor(readonly cells: Cell[][]) {
  }

  getCellAt(location: CellLocation): Cell | undefined {

    const row = this.cells[location.y];

    return !!row ? row[location.x] : undefined;
  }

  setCellAt(location: CellLocation, newCell: Cell): void {

    this.cells[location.y][location.x] = newCell;
  }

  getAllCells(): Map<CellLocation, Cell> {

    const items: ([CellLocation, Cell])[] = [];

    this.cells.forEach(
      (row, rowIndex) => row.forEach(
        (cell, columnIndex) =>
          items.push([new CellLocation(columnIndex, rowIndex), cell])));

    return new Map(items);
  }
}

export interface GameConfiguration {
  readonly constructionSpecifications: ConstructionSpecifications
}

export interface ThroughputSpecifications {
  getResourcesSpecificationForType(type: BuildingType): ResourcesSpecification;
}

export interface ThroughputSpecification {
  readonly input: ResourcesSpecification;
  readonly output: ResourcesSpecification;
}

export interface ConstructionSpecification {
  readonly duration: number;
}

export interface ConstructionSpecifications {
  getSpecificationForType(type: BuildingType): ConstructionSpecification;
}

export class ConstructionSpecificationsMap implements ConstructionSpecifications {

  private readonly map = new Map<BuildingType, ConstructionSpecification>();

  constructor(...items: ([BuildingType, ConstructionSpecification])[]) {
    items.forEach(([type, spec]) => this.map.set(type, spec));
  }

  getSpecificationForType(type: BuildingType): ConstructionSpecification {
    const spec = this.map.get(type);
    if (!spec) {
      throw new Error("Missing construction specification for building type: " + type);
    }
    return spec;
  }
}

export interface Cell {
  readonly buildingType: BuildingType;
  readonly constructionSite?: ConstructionSite;
}

export function startNewGame(configuration: GameConfiguration): Game {

  setBuildingConfiguration(
    BuildingType.B_POND,
    new BuildingConfigurationBuilder()
      .throughputTotals()
      .inputs()
      .add(R_ENERGY_ELECTRICAL, 4)
      .outputs()
      .add(R_WATER, 1)
      .build()
  );

  const game: Game = {
    map: emptyCellMap(),
    configuration
  };

  const capsule = createCell(BuildingType.B_TERMINAL);


  setCellAt(game, new CellLocation(getRandomCoord(), getRandomCoord()), capsule);

  selectCell(game, new CellLocation(0, 0));

  return game;
}

function emptyCellMap(): CellMap {

  return createCellMap(
    MAP_SIZE,
    () => createCell(BuildingType.B_TERRAIN)
  );
}

function createCellMap(size, cellFactory: () => Cell): CellMap {

  const matrix: Cell[][] = [];

  for (let x = 0; x < size; x++) {

    const row: Cell[] = [];

    matrix.push(row);

    for (let y = 0; y < size; y++) {
      row.push(cellFactory());
    }
  }

  return new CellMap(matrix);
}

export function getRandomCoord(): number {
  return Math.floor(Math.random() * MAP_SIZE);
}

