import {INITIAL_CAPSULE_STORAGE, setConstructablesOnCellType, setInitialMissingResource} from "./config";
import {R_CONSTRUCTION_BLOCK, R_ELECTRONICS, R_PIPES, R_SOLAR_PANEL} from "./resource-types";
import {BuildingType} from "./building-types";

export function createInitialResources() {

  setInitialMissingResource(BuildingType.B_GARDEN, R_SOLAR_PANEL, 5);
  setInitialMissingResource(BuildingType.B_GARDEN, R_ELECTRONICS, 4);
  setInitialMissingResource(BuildingType.B_GARDEN, R_CONSTRUCTION_BLOCK, 5);

  setInitialMissingResource(BuildingType.B_POND, R_PIPES, 5);
  setInitialMissingResource(BuildingType.B_POND, R_ELECTRONICS, 2);
  setInitialMissingResource(BuildingType.B_POND, R_CONSTRUCTION_BLOCK, 5);

  INITIAL_CAPSULE_STORAGE.set(R_CONSTRUCTION_BLOCK, 100);
  INITIAL_CAPSULE_STORAGE.set(R_PIPES, 100);
  INITIAL_CAPSULE_STORAGE.set(R_ELECTRONICS, 200);
  INITIAL_CAPSULE_STORAGE.set(R_SOLAR_PANEL, 100);

  setConstructablesOnCellType(
    BuildingType.B_TERRAIN,
    BuildingType.B_POND, BuildingType.B_GARDEN
  );
}