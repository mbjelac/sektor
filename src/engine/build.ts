import {setCellAt} from "./cells";
import {selectCell} from "./select-cell";
import {createConstructionSite} from "./create-cell";
import {BuildingType} from "./building-types";
import {Cell, Game} from "./start-new-game";

export function build(game: Game, buildingType: BuildingType): void {

  const selectedLocation = game.selectedLocation;

  if (!selectedLocation) {
    throw new Error("No selected cell!");
  }

  const selectedCell = game.map.getCellAt(selectedLocation);

  selectedCellMustBeEmpty(selectedCell!);

  const newCell = createConstructionSite(buildingType, game.configuration.constructionSpecifications);

  setCellAt(game, selectedLocation, newCell);

  selectCell(game, selectedLocation);
}

function selectedCellMustBeEmpty(selectedCell: Cell) {
  if (selectedCell.buildingType !== BuildingType.B_TERRAIN) {
    throw new Error('Cell is occupied!');
  }
}
