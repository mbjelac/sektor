export enum BuildingType {
  B_TERRAIN = 'terrain',
  B_TERMINAL = 'terminal',
  B_GARDEN = 'garden',
  B_CONSTRUCTION = 'construction',
  B_POND = 'pond',
  B_SPARK_CATCHER = "spark-catcher",
  B_HIVE = "hive",
}
