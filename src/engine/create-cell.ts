import {BuildingType} from "./building-types";
import {BuildingConfiguration, getBuildingConfiguration} from "./config";
import {Cell, ConstructionSpecifications} from "./start-new-game";
import {Resources} from "./cell-processors/resources";
import {ConstructionSite} from "./construction/construction-site";

export function createConstructionSite(buildingType, specs: ConstructionSpecifications): Cell {

  if (buildingType === BuildingType.B_CONSTRUCTION) {
    throw new Error('Building type not allowed: ' + BuildingType.B_CONSTRUCTION);
  }

  return {
    buildingType: BuildingType.B_CONSTRUCTION,
    constructionSite: new ConstructionSite(
      buildingType,
      specs.getSpecificationForType(buildingType).duration
    ),
  };
}

export function createCell(type: BuildingType): Cell {

  const config: BuildingConfiguration = getBuildingConfiguration(type);

  return {
    buildingType: type,
  }
}

function createCellProperties(type: string) {

  switch (type) {
    default:
      return {};
  }

}
