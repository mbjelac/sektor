import {CellLocation} from "./cells";
import {processConstructionSite} from "./cell-processors/process-construction-site";
import {BuildingType} from "./building-types";
import {CellMap, Game} from "./start-new-game";

export function tick(game: Game) {

  game.map.getAllCells()
    .forEach((cell, location) => processCell(game.map, location));
}

function processCell(cellMap: CellMap, location: CellLocation) {

  const processor = getCellProcessor(cellMap.getCellAt(location)!.buildingType);

  processor(cellMap, location);
}

const noOpCellProcessor = () => {
};

const cellProcessors = new Map<BuildingType, Processor>()
  .set(BuildingType.B_CONSTRUCTION, processConstructionSite)
  .set(BuildingType.B_TERRAIN, noOpCellProcessor)
  .set(BuildingType.B_TERMINAL, noOpCellProcessor)
  .set(BuildingType.B_POND, noOpCellProcessor)
  .set(BuildingType.B_GARDEN, noOpCellProcessor)
;

type Processor = (map: CellMap, cellLocation: CellLocation) => void;

function getCellProcessor(type: BuildingType): Processor {

  const processor = cellProcessors.get(type);

  if (processor) {
    return processor;
  }

  throw new Error('Cell processor not defined for cell type: ' + type);
}
