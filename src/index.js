import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import {startTicking} from "./ui/ticker";
import {App} from "./ui/App";

ReactDOM.render(<App/>, document.getElementById('root'));
registerServiceWorker();

startTicking();
