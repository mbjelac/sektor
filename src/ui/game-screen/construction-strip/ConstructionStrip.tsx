import React from "react";
import {Cell, CellMap} from "../../../engine/start-new-game";
import {BuildingType} from "../../../engine/building-types";
import {IsometricGrid} from "../isometric-grid/IsometricGrid";
import {ScreenPoint} from "../isometric-grid/isometric-coord-mapper";
import {CellLocation} from "../../../engine/cells";

export interface ConstructionStripProps {
  readonly cellSizePx: number,
  readonly offsetPoint: ScreenPoint,
  readonly buildingTypes: BuildingType[];
  readonly typeSelectionListener: (selected: BuildingType) => void
}

export const ConstructionStrip: React.FC<ConstructionStripProps> = (props: ConstructionStripProps) => {

  return (
    <IsometricGrid
      contents={cellMapFromTypes(props.buildingTypes)}
      cellSizePx={props.cellSizePx}
      offsetPoint={props.offsetPoint}
      cellClickListener={handleCellClick}
    />
  );

  function handleCellClick(location: CellLocation): void {

    if (location.y !== 0) {
      return;
    }

    const index = location.x;

    const selectedType = props.buildingTypes[index];

    if (selectedType) {
      props.typeSelectionListener(selectedType);
    }
  }
}

function cellMapFromTypes(types: BuildingType[]): CellMap {
  return new CellMap(
    [types.map(cellFromType)]
  );
}

function cellFromType(type: BuildingType): Cell {
  return { buildingType: type }
}