import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import {Meta, Story} from '@storybook/react/types-6-0';
import {ConstructionStrip} from "./ConstructionStrip";
import {STORY_TITLES} from "../../story-titles";
import {story} from "../../story-utils";
import {action} from "@storybook/addon-actions";
import {BuildingType} from "../../../engine/building-types";


export default {
  title: STORY_TITLES.get(ConstructionStrip),
  component: ConstructionStrip,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

interface StoryProps {
  types: BuildingType[]
}

const Template: Story<StoryProps> = (args) => (
  <ConstructionStrip
    cellSizePx={50}
    offsetPoint={{ x: 200, y: 50 }}
    buildingTypes={args.types}
    typeSelectionListener={action("click")}
  />
);

export const SomeConstructionStrip = story(
  Template,
  { types: [BuildingType.B_GARDEN, BuildingType.B_POND, BuildingType.B_TERMINAL] }
);
