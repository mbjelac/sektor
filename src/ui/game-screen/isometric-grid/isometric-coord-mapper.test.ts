import {ScreenPoint, toIsometricCoords, toMatrixCoords} from "./isometric-coord-mapper";
import {CellLocation} from "../../../engine/cells";

/*

 NOTE: screen input coordinates & sizes taken from hand-made isometric mockup, see: test-data/isometric-test-data-grid.bmp

  a b c d
  e f g h
  i j k l
  m n o p

  -->

      a
     e b
    i f c
   m j g d
    n k h
     o l
      p

   */


interface Example {
  matrixIndexes: [number, number];
  screenCoordinates: [number, number];
}

const offset: ScreenPoint = { x: 916, y: 45 };
const tileSize = 48;

describe('map matrix coords to isometric space', () => {

  const examples: Example[] = [
    { matrixIndexes: [0, 0], screenCoordinates: [916, 45] },

    { matrixIndexes: [0, 1], screenCoordinates: [868, 69] },
    { matrixIndexes: [0, 2], screenCoordinates: [820, 93] },
    { matrixIndexes: [0, 3], screenCoordinates: [772, 117] },

    { matrixIndexes: [1, 0], screenCoordinates: [964, 69] },
    { matrixIndexes: [2, 0], screenCoordinates: [1012, 93] },
    { matrixIndexes: [3, 0], screenCoordinates: [1060, 117] },

    { matrixIndexes: [9, 9], screenCoordinates: [916, 477] },
    { matrixIndexes: [9, 0], screenCoordinates: [1348, 261] },
    { matrixIndexes: [0, 9], screenCoordinates: [484, 261] },

    { matrixIndexes: [3, 4], screenCoordinates: [868, 213] },
    { matrixIndexes: [8, 5], screenCoordinates: [1060, 357] },
    { matrixIndexes: [1, 6], screenCoordinates: [676, 213] },
  ];

  examples.forEach(example =>
    it(JSON.stringify(example), () => {

      const [x, y] = example.matrixIndexes;
      const [sx, sy] = example.screenCoordinates;

      expect(toIsometricCoords(new CellLocation(x, y), tileSize, offset)).toEqual({ x: sx, y: sy });
    }));
});

describe('maps isometric coordinates to matrix indices', function () {

  const examples: Example[] = [

    // offset point
    { screenCoordinates: [916, 45], matrixIndexes: [0, 0] },

    // 1st row, centers
    { screenCoordinates: [917, 69], matrixIndexes: [0, 0] },
    { screenCoordinates: [967, 94], matrixIndexes: [1, 0] },
    { screenCoordinates: [1013, 119], matrixIndexes: [2, 0] },
    { screenCoordinates: [1060, 141], matrixIndexes: [3, 0] },
    { screenCoordinates: [1107, 166], matrixIndexes: [4, 0] },
    { screenCoordinates: [1159, 189], matrixIndexes: [5, 0] },
    { screenCoordinates: [1205, 211], matrixIndexes: [6, 0] },
    { screenCoordinates: [1253, 238], matrixIndexes: [7, 0] },

    // 1st row, near borders
    { screenCoordinates: [930, 75], matrixIndexes: [0, 0] },
    { screenCoordinates: [949, 86], matrixIndexes: [1, 0] },

    // 2nd row, centers
    { screenCoordinates: [866, 94], matrixIndexes: [0, 1] },
    { screenCoordinates: [916, 117], matrixIndexes: [1, 1] },
    { screenCoordinates: [967, 140], matrixIndexes: [2, 1] },

    // various points within tile at 7,6
    { screenCoordinates: [965, 381], matrixIndexes: [7, 6] }, // approx center
    { screenCoordinates: [954, 370], matrixIndexes: [7, 6] }, // near left
    { screenCoordinates: [947, 391], matrixIndexes: [7, 6] }, // near bottom
    { screenCoordinates: [984, 390], matrixIndexes: [7, 6] }, // near right
    { screenCoordinates: [986, 373], matrixIndexes: [7, 6] }, // near top
  ];

  examples.forEach(example => it(JSON.stringify(example), () => {

    const [x, y] = example.screenCoordinates;
    const [mx, my] = example.matrixIndexes;

    const point: ScreenPoint = { x, y };

    expect(toMatrixCoords(point, tileSize, offset)).toEqual(new CellLocation(mx, my));
  }));
});