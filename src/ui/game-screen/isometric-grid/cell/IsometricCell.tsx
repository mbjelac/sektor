import React, {CSSProperties, Fragment} from "react";
import {Cell} from "../../../../engine/start-new-game";
import {BuildingType} from "../../../../engine/building-types";
import image_selection_cube_back from "./selection_cube_back.png";
import image_selection_cube_front from "./selection_cube_front.png";
import image_terrain from "./building-images/terrain.png";
import {CellImage} from "./CellImage";

export interface IsometricCellProps {
  selected: boolean;
  cell: Cell;
}

const cellPosition: CSSProperties = {
  position: 'absolute',
  top: '0px',
  left: '0px'
};

const backSelectionCubePosition: CSSProperties = {
  position: 'absolute',
  top: '-24px',
  left: '0px'
};

const frontSelectionCubePosition: CSSProperties = {
  position: 'absolute',
  top: '0px',
  left: '0px'
};

export const IsometricCell: React.FC<IsometricCellProps> = (props: IsometricCellProps) => {

  const isSelected = props.selected;
  const type = props.cell.buildingType;

  return <div>
    <img
      style={cellPosition}
      src={image_terrain}
      alt='empty'
    />
    {isSelected
      ? <img
        style={backSelectionCubePosition}
        src={image_selection_cube_back}
        alt='selector'
      />
      : <Fragment/>
    }
    {type !== BuildingType.B_TERRAIN
      ? <div style={getBuildingPosition(type)}>
        <CellImage type={type}/>
      </div>
      : <Fragment/>
    }
    {isSelected
      ? <img
        style={frontSelectionCubePosition}
        src={image_selection_cube_front}
        alt='selector'
      />
      : <Fragment/>
    }
  </div>;
};

function getBuildingPosition(type: BuildingType): CSSProperties {
  return {
    position: 'absolute',
    top: `-${topOffsetByType.get(type) || 0}px`,
    left: '0px'
  };
}

const topOffsetByType = new Map<BuildingType, number>()
  .set(BuildingType.B_TERMINAL, 51)
  .set(BuildingType.B_GARDEN, 7)
  .set(BuildingType.B_CONSTRUCTION, 14)
;
