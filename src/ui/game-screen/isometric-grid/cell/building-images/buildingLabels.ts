export const buildingLabels = {
  pond: "Pond",
  garden: "Garden",
  terminal: "Terminal",
  terrain: "Terrain",
  construction: "Construction"
}