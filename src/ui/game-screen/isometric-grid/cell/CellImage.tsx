import React from "react";
import {BuildingType} from "../../../../engine/building-types";

import unknownBuilding from "./building-images/unknown.png";


export interface CellImageProps {
  type: BuildingType
}

export const CellImage: React.FC<CellImageProps> = (props: CellImageProps) => {

  return (
    <div>
      <img
        src={loadImage(props.type)}
        alt={props.type}
      />
    </div>
  );
};

const imageCache = new Map<string, any>();

function loadImage(name: string): any {

  const image = imageCache.get(name);

  if (!!image){
    return image;
  }

  const newImage = loadNewImage(name);
  imageCache.set(name, newImage);

  return newImage;
}

function loadNewImage(name: string): any {
  try {
    return require(`./building-images/${name}.png`);
  } catch (error) {
    return unknownBuilding;
  }
}