import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import {Meta, Story} from '@storybook/react/types-6-0';
import {IsometricCell, IsometricCellProps} from "./IsometricCell";
import {BuildingType} from "../../../../engine/building-types";
import {STORY_TITLES} from "../../../story-titles";
import {Resources} from "../../../../engine/cell-processors/resources";
import {Cell} from "../../../../engine/start-new-game";

export default {
  title: STORY_TITLES.get(IsometricCell),
  component: IsometricCell,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<IsometricCellProps> = (args) => <IsometricCell {...args} />;

const dummyCell: Cell = { buildingType: BuildingType.B_TERRAIN }

export const EmptyUnselected = Template.bind({}, {
  cell: {
    ...dummyCell,
    buildingType: BuildingType.B_TERRAIN,
  },
  selected: false
});

export const EmptySelected = Template.bind({}, {
  cell: {
    ...dummyCell,
    buildingType: BuildingType.B_TERRAIN
  },
  selected: true
});

export const OccupiedUnselected = Template.bind({}, {
  cell: {
    ...dummyCell,
    buildingType: BuildingType.B_TERMINAL
  },
  selected: false
});

export const OccupiedSelected = Template.bind({}, {
  cell: {
    ...dummyCell,
    buildingType: BuildingType.B_TERMINAL
  },
  selected: true
});
