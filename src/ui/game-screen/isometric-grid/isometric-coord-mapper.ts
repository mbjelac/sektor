import {CellLocation} from "../../../engine/cells";

export interface ScreenPoint {
  x: number;
  y: number;
}

export function toIsometricCoords(cellLocation: CellLocation, cartesianTileSize: number, isometricOffset: ScreenPoint): ScreenPoint {

  const rotated: ScreenPoint = {
    x: cellLocation.x - cellLocation.y,
    y: cellLocation.x + cellLocation.y
  };

  return {
    x: isometricOffset.x + rotated.x * cartesianTileSize,
    y: isometricOffset.y + rotated.y * cartesianTileSize / 2
  }
}

export function toMatrixCoords(screenLocation: ScreenPoint, cartesianTileSize: number, isometricOffset: ScreenPoint): CellLocation {
  const cartesianPoint = toCartesianCoords({
    x: screenLocation.x - isometricOffset.x,
    y: screenLocation.y - isometricOffset.y
  });
  return new CellLocation(
    Math.floor(cartesianPoint.x / cartesianTileSize),
    Math.floor(cartesianPoint.y / cartesianTileSize)
  );
}

function toCartesianCoords(isometricPoint: ScreenPoint): ScreenPoint {
  return {
    x: (2 * isometricPoint.y + isometricPoint.x) / 2,
    y: (2 * isometricPoint.y - isometricPoint.x) / 2
  };
}
