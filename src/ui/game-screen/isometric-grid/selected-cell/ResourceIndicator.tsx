import React from "react";
import {ResourceGauge} from "./ResourceGauge";
import {ResourceIcon} from "./resource-icon/ResourceIcon";
import {ResourceState, ResourceType} from "../../../../engine/cell-processors/resources";

export interface ResourceIndicatorProps {
  type: ResourceType;
  state: ResourceState;
  orientation: ResourceIndicatorOrientation;
}

export enum ResourceIndicatorOrientation {
  INPUT = "INPUT",
  OUTPUT = "OUTPUT"
}

export const ResourceIndicator: React.FC<ResourceIndicatorProps> = (props: ResourceIndicatorProps) => {

  const reverseLayout = props.orientation === ResourceIndicatorOrientation.OUTPUT;

  return (
    <div style={{
      display: "flex",
      alignItems: "center"
    }}>
      {layout(
        <ResourceGauge amount={props.state.amount} total={props.state.total}/>,
        <div style={{ paddingLeft: "4px" }}/>,
        <ResourceIcon resourceType={props.type}/>,
        reverseLayout
      )}
    </div>
  );
};

function layout(left, middle, right, reverse: boolean) {
  return reverse
    ? <React.Fragment>{right}{middle}{left}</React.Fragment>
    : <React.Fragment>{left}{middle}{right}</React.Fragment>;
}