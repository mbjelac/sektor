import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import {Meta, Story} from '@storybook/react/types-6-0';
import {ResourceIndicators, ResourceIndicatorsProps} from "./ResourceIndicators";
import {someResourceType} from "./test-data";
import {STORY_TITLES} from "../../../story-titles";
import {createResources, story} from "../../../story-utils";
import {ResourceIndicatorOrientation} from "./ResourceIndicator";


export default {
  title: STORY_TITLES.get(ResourceIndicators),
  component: ResourceIndicators,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;


const Template: Story<ResourceIndicatorsProps> = (args) => (<ResourceIndicators {...args} />);

export const InputPanel = story(
  Template,
  {
    resources: createResources(
      [someResourceType.water, 12, 0],
      [someResourceType.electricity, 5, 1],
      [someResourceType.food, 8, 5],
    ),
    orientation: ResourceIndicatorOrientation.INPUT
  }
);

export const OutputPanel = story(
  Template,
  {
    resources: createResources(
      [someResourceType.water, 7, 1],
      [someResourceType.food, 18, 15],
    ),
    orientation: ResourceIndicatorOrientation.OUTPUT
  }
);
