import {getPercentage} from "./ResourceGauge";

describe("Get percentage", () => {

  const examples: ([number, number, number])[] = [
    [0, 0, 0],
    [0, 10, 0],
    [1, 10, 10],
    [2, 10, 20],
    [2, 20, 10],
    [2, 6, 33],
    [12, 10, 100],
    [133412, 150, 100],
    [17, 0, 0],
    [-1, 10, 0],
    [-10, 10, 0],
    [-100, 10, 0],
    [1, -10, 0],
    [12, -10, 0],
    [-1, -10, 0],
    [156, -10, 0],
    [-1, -1, 0],
    [-12, -170, 0],
  ];

  examples.forEach(example => it(JSON.stringify(example), () => {

    const [amount, total, expectedPercentage] = example;

    expect(getPercentage({ amount, total })).toEqual(expectedPercentage);
  }));
});