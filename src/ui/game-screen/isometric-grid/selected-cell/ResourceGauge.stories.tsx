import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import {ResourceGauge, ResourceGaugeProps} from "./ResourceGauge";
import {STORY_TITLES} from "../../../story-titles";


export default {
  title: STORY_TITLES.get(ResourceGauge),
  component: ResourceGauge,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<ResourceGaugeProps> = (args) => <ResourceGauge {...args} />;

export const Empty = Template.bind({});
Empty.args = {
  total: 177,
  amount: 0
};

export const Full = Template.bind({});
Full.args = {
  total: 234,
  amount: 234
};

export const HalfFull = Template.bind({});
HalfFull.args = {
  total: 150,
  amount: 75
};

export const AlmostFull = Template.bind({});
AlmostFull.args = {
  total: 8,
  amount: 7
};

export const AlmostEmpty = Template.bind({});
AlmostEmpty.args = {
  total: 8,
  amount: 1
};
