import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import {Meta, Story} from '@storybook/react/types-6-0';
import {SelectedCell, SelectedCellProps} from "./SelectedCell";
import {BuildingType} from "../../../../engine/building-types";
import {someResourceType} from "./test-data";
import {STORY_TITLES} from "../../../story-titles";
import {createResources, story} from "../../../story-utils";


export default {
  title: STORY_TITLES.get(SelectedCell),
  component: SelectedCell,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;


const Template: Story<SelectedCellProps> = (args) => (
  <div style={{ display: "flex", flexDirection: "row", background: "black" }}>
    <SelectedCell {...args} />
    <div style={{ flexGrow: 1 }}/>
  </div>
);

export const Garden = story(
  Template,
  {
    selectedCell: {
      buildingType: BuildingType.B_GARDEN,
    }
  });

export const Pond = story(
  Template,
  {
    selectedCell: {
      buildingType: BuildingType.B_POND,
    }
  });

export const Empty = story(
  Template,
  {
    selectedCell: {
      buildingType: BuildingType.B_TERRAIN,
    }
  });

export const Terminal = story(
  Template,
  {
    selectedCell: {
      buildingType: BuildingType.B_TERMINAL,
    }
  });
