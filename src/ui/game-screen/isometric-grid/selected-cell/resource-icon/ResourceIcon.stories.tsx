import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import {Meta, Story} from '@storybook/react/types-6-0';
import {ResourceIcon, ResourceIconProps} from "./ResourceIcon";
import {STORY_TITLES} from "../../../../story-titles";


export default {
  title: STORY_TITLES.get(ResourceIcon),
  component: ResourceIcon,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<ResourceIconProps> = (args) => <ResourceIcon {...args} />;

export const Water = Template.bind({});
Water.args = {
  resourceType: "water"
};

export const Missing = Template.bind({});
Missing.args = {
  resourceType: "missing type"
};
