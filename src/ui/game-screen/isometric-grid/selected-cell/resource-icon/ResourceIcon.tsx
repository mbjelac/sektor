import React from "react";

import defaultIcon from "./icons/unknown.png";
import {ResourceType} from "../../../../../engine/cell-processors/resources";

export interface ResourceIconProps {
  resourceType: ResourceType;
}

export const ResourceIcon: React.FC<ResourceIconProps> = (props: ResourceIconProps) => {

  return (
    <div>
      <img
        src={loadIcon(props.resourceType)}
        alt={props.resourceType}
      />
    </div>
  );
};

function loadIcon(name: string): any {
  try {
    return require(`./icons/${name}.png`);
  } catch (error) {
    return defaultIcon;
  }
}