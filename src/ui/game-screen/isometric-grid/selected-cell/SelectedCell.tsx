import React, {CSSProperties} from "react";
import {CellImage} from "../cell/CellImage";
import {ResourceIndicators} from "./ResourceIndicators";
import {buildingLabels} from "../cell/building-images/buildingLabels";
import {Cell} from "../../../../engine/start-new-game";
import {ResourceIndicatorOrientation} from "./ResourceIndicator";

export interface SelectedCellProps {
  selectedCell: Cell
}

const containerResources: CSSProperties = {
  width: "70px",
};

const containerCell: CSSProperties = {
  width: "100px",
};

export const SelectedCell: React.FC<SelectedCellProps> = (props: SelectedCellProps) => {

  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <div style={{ display: "flex", flexDirection: "row", height: "100px", alignItems: "flex-end" }}>
        <div style={containerCell}>
          <CellImage type={props.selectedCell.buildingType}/>
        </div>
      </div>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <span style={{
          textTransform: "uppercase",
          fontFamily: "monospace",
          color: "yellow"
        }}>{buildingLabels[props.selectedCell.buildingType]}</span>
      </div>
    </div>
  );
};
