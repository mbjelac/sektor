import React, {CSSProperties} from "react";

const gaugeBackground: CSSProperties = {
  background: "aliceblue",
  height: "10px",
  width: "40px"
};

function gaugeForeground(percentage: number): CSSProperties {
  return {
    height: "100%",
      width: `${percentage}%`,
    background: "cadetblue"
  }
}

export interface ResourceGaugeProps {
  amount: number;
  total: number;
}

export const ResourceGauge: React.FC<ResourceGaugeProps> = (props: ResourceGaugeProps) => {

  const percentage = getPercentage(props);

  return (
    <div style={gaugeBackground}>
      <div style={gaugeForeground(percentage)}/>
    </div>
  );
};

export function getPercentage(props: ResourceGaugeProps): number {

  if (props.total <= 0 || props.amount <= 0) {
    return 0;
  }

  if (props.amount > props.total) {
    return 100;
  }

  return Math.floor(100 * props.amount / props.total);
}