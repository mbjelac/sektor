import React from "react";
import {ResourceIndicator, ResourceIndicatorOrientation} from "./ResourceIndicator";
import {Resources} from "../../../../engine/cell-processors/resources";

export interface ResourceIndicatorsProps {
  resources: Resources;
  orientation: ResourceIndicatorOrientation;
}

export const ResourceIndicators: React.FC<ResourceIndicatorsProps> = (props: ResourceIndicatorsProps) => {

  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      {props.resources.getAll().map(typedState =>
        (
          <ResourceIndicator
            type={typedState.type}
            state={typedState.state}
            orientation={props.orientation}
          />
        ))}
    </div>
  );
};
