import React from "react";
import {Cell, CellMap} from "../../../engine/start-new-game";
import {CellLocation} from "../../../engine/cells";
import {ScreenPoint, toIsometricCoords, toMatrixCoords} from "./isometric-coord-mapper";
import {IsometricCell} from "./cell/IsometricCell";

let nextKey = 0;

function createKey() {
  return nextKey++;
}

export interface IsometricGridProps {
  contents: CellMap,
  selectedLocation?: CellLocation,
  cellSizePx: number,
  offsetPoint: ScreenPoint,
  cellClickListener: (clickedLocation: CellLocation) => void
}

export const IsometricGrid: React.FC<IsometricGridProps> = (props: IsometricGridProps) => {

  return (
    <div>
      <div style={{ position: 'relative' }}>
        {
          props.contents
            .cells.map((row, rowIndex) =>
            row.map((cell, columnIndex) =>
              renderCellInMap(cell, new CellLocation(columnIndex, rowIndex))))
        }
      </div>
    </div>
  )

  function renderCellInMap(cell: Cell, location: CellLocation) {

    const screenPoint = toIsometricCoords(location, props.cellSizePx, props.offsetPoint);

    return (
      <div
        key={createKey()}
        onClick={cellClicked}
        style={{
          position: 'absolute',
          top: `${screenPoint.y}px`,
          left: `${screenPoint.x - props.cellSizePx}px`
        }}
      >
        <IsometricCell cell={cell} selected={location.isEqual(props.selectedLocation)}/>
      </div>
    );
  }

  function cellClicked(event): void {

    const clickPoint: ScreenPoint = {
      x: event.clientX,
      y: event.clientY
    };

    const cellLocation = toMatrixCoords(clickPoint, props.cellSizePx, props.offsetPoint);

    props.cellClickListener(cellLocation);
  }
};
