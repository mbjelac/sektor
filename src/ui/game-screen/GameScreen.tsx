import React, {CSSProperties, Fragment} from 'react';
import {getConstructableBuildingTypes, getSelectedCell} from "../../engine/tool-strip";
import {refresh} from "../App";
import {CellLocation} from "../../engine/cells";
import {SelectedCell} from "./isometric-grid/selected-cell/SelectedCell";
import {IsometricGrid} from "./isometric-grid/IsometricGrid";
import {BuildingType} from "../../engine/building-types";
import {build} from "../../engine/build";
import {selectCell} from "../../engine/select-cell";
import {gameState} from "../../game-state";
import {ConstructionStrip} from "./construction-strip/ConstructionStrip";

const CELL_SIZE_PX = 50;

export function GameScreen() {

  if (!gameState.game) {
    return (<div>Game not initialised!</div>);
  }

  const selectedCell = getSelectedCell(gameState.game);

  const selectedCellPanel = selectedCell
    ? (
      <div style={position(0, 0)}>
        <SelectedCell selectedCell={selectedCell}/>
      </div>
    )
    : <Fragment/>;

  const constructableTypes = getConstructableBuildingTypes(gameState.game);

  const constructionStrip = constructableTypes.length > 0
    ? (
      <ConstructionStrip
        cellSizePx={CELL_SIZE_PX}
        offsetPoint={{ x: 900, y: 20 }}
        buildingTypes={constructableTypes}
        typeSelectionListener={constructCellClicked}
      />
    )
    : <Fragment/>;

  return (
    <div style={{ position: "relative" }}>

      <div style={position(0, 0)}>
        <IsometricGrid
          contents={gameState.game.map}
          selectedLocation={gameState.game.selectedLocation}
          cellSizePx={CELL_SIZE_PX}
          offsetPoint={{ x: 550, y: 20 }}
          cellClickListener={mapCellClicked}
        />
      </div>

      {selectedCellPanel}
      {constructionStrip}
    </div>
  );
}

function position(x: number, y: number): CSSProperties {
  return { position: "absolute", left: `${x}px`, top: `${y}px` };
}

function mapCellClicked(location: CellLocation): void {
  selectCell(gameState.game!, location);
  refresh();
}

function constructCellClicked(type: BuildingType): void {
  build(gameState.game!, type);
  refresh();
}
