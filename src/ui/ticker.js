import {gameState} from "../game-state";
import {tick} from "../engine/tick";
import {refresh} from "./App";

export function startTicking() {
  setInterval(() => doTick(), 1000);
}

function doTick() {
    if (gameState.game) {
        tick(gameState.game);
        refresh();
    }
}
