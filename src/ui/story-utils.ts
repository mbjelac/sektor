import {Resources, ResourceType} from "../engine/cell-processors/resources";
import {Story} from "@storybook/react/types-6-0";

export function createResources(...resourceData: [ResourceType, number, number][]): Resources {

  const resourceDefinition: [ResourceType, number][] = resourceData.map(([type, total,]) => [type, total]);

  const resources = Resources.from(...resourceDefinition);

  resourceData.forEach(([type, , current]) => resources.change(type, current));

  return resources;
}

export function story<T>(template: Story<T>, args: T) {
  const Story = template.bind([]);
  Story.args = args;
  return Story;
}