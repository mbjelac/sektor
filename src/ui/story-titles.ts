import {ResourceGauge} from "./game-screen/isometric-grid/selected-cell/ResourceGauge";
import {ResourceIndicators} from "./game-screen/isometric-grid/selected-cell/ResourceIndicators";
import {SelectedCell} from "./game-screen/isometric-grid/selected-cell/SelectedCell";
import {ResourceIcon} from "./game-screen/isometric-grid/selected-cell/resource-icon/ResourceIcon";
import React from "react";
import {IsometricCell} from "./game-screen/isometric-grid/cell/IsometricCell";
import {ConstructionStrip} from "./game-screen/construction-strip/ConstructionStrip";

export const STORY_TITLES = new Map<React.FC<any>, string>()
  .set(IsometricCell, 'Map/Cell')
  .set(ResourceGauge, 'Selection/Cell State/Resource Indicators/Gauge')
  .set(ResourceIndicators, 'Selection/Cell State/Resource Indicators')
  .set(SelectedCell, 'Selection/Cell State')
  .set(ResourceIcon, 'Selection/Cell State/Resource Indicators/Icon')
  .set(ConstructionStrip, 'Selection/Construction Strip')
;