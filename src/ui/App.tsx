import {Component} from 'react';
import './App.css';
import {gameState} from "../game-state";
import {ConstructionSpecificationsMap, startNewGame} from "../engine/start-new-game";
import {createInitialResources} from "../engine/game-setup";
import {GameScreen} from "./game-screen/GameScreen";
import {BuildingType} from "../engine/building-types";
import React from 'react';

let app;

export function refresh() {
  if (app) {
    app.setState(gameState);
  }
}

export class App extends Component {

  constructor(props) {

    super(props);

    app = this;

    createInitialResources();

    gameState.game = startNewGame({
      constructionSpecifications: new ConstructionSpecificationsMap(
        [BuildingType.B_GARDEN, { duration: 5 }],
        [BuildingType.B_POND, { duration: 3 }],
        [BuildingType.B_SPARK_CATCHER, { duration: 8 }],
        [BuildingType.B_HIVE, { duration: 10 }],
      )
    });
  }

  render() {
    return <GameScreen />;
  }
}