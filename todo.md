# TODO

## Pilot

### Goals

- multiplayer (inter-city contracts)
- cities (no regions, world)

### Features

- player
    - optional player name
    - no password, just UUID (secret)
    - authentication with UUID
    - create ONE city
    - automatic login after initial login
- city map
    - build buildings (i.e. processors)
    - connect buildings (i.e. create contracts)
    - view building state (I/O buffers)
    - view/modify contracts
    - build terminal (i.e. inter-city I/O)
    - configure buildings (tweak processor parameters - if applicable)
    - configure terminal (inter-city contracts)
    
